import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './auth/login/login.component';
import { AuthComponent } from './auth/auth.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { PortalComponent } from './portal/portal.component';
import { DashboardComponent } from './portal/dashboard/dashboard.component';
import { SearchResultComponent } from './portal/search-result/search-result.component';
import { CaseComponent } from './portal/case/case.component';
import { CaseListComponent } from './portal/case/case-list/case-list.component';
import { CaseNewComponent } from './portal/case/case-new/case-new.component';

const appRoutes: Routes = [
  { path: 'auth', component: AuthComponent, children: [
      { path: 'login', component: LoginComponent },
      { path: 'forgot', component: ForgotPasswordComponent }
    ]
  },
  { path: '', component: PortalComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'case', component: CaseComponent , children: [
      { path: '', component: CaseListComponent },
      { path: 'new', component: CaseNewComponent },
      { path: '**', redirectTo: '' }
    ]
    },
    { path: 'search-result', component: SearchResultComponent },
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
