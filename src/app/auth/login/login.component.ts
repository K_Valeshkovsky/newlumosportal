import { Component, OnInit} from '@angular/core';
import { FormControl, Validators, FormGroupDirective, NgForm, FormGroup, FormBuilder } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import { ActivatedRoute, Router } from 'node_modules/@angular/router';

/** Error when invalid control is dirty, touched, or submitted. */

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  email: string = 'example@example.com';
  password: string;
  location: string;
  formSubmitted: Boolean = false;
  rememberMe: Boolean = false;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router) { }
  

  isControlInvalid(controlName: string): boolean {
    const control = this.loginForm.controls[controlName];
    const result = this.formSubmitted && control.invalid && control.touched;
    return result;
  }
  
  ngOnInit() {
    this.loginForm = this.fb.group({
      'email': [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
        ])
      ],
      'location' : null,
      'password' : [
        null, 
        Validators.compose([
          Validators.required,
          Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
        ])
      ],
      'rememberMe': false
    })
    
  }

  onFormSubmit(form) {
    this.formSubmitted = true;
    const controls = this.loginForm.controls;

    if (this.loginForm.invalid) {
      Object.keys(controls).forEach(controlName => controls[controlName].markAsTouched());
      return;
    }

    console.log(this.loginForm.value);
    this.router.navigate(['/']);
  } 

}