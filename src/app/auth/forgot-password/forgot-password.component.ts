import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '../../../../node_modules/@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  public forgotForm: FormGroup;
  email: string;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.forgotForm = this.fb.group({
      'email': [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
        ])
      ]
    })
  }

  onFormSubmit(form:NgForm) {  
    console.log(form);  
  } 
}
