import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../environments/environment';
declare var getSfStaticResourceUrl: () => string;

export function getStaticPathForResource(resourcePath: string) {
  return getSfStaticResourceUrl() + '/' + resourcePath;
}

@Pipe({
  name: 'staticPath'
})
export class StaticPathPipe implements PipeTransform {

  transform(value: string): string {
    return getStaticPathForResource(value);
    // return value;
  }

}
