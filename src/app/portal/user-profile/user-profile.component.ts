import {
  Component,
  OnInit
} from '@angular/core';

import { MenuItem } from 'primeng/api';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  menuItems: MenuItem[];
  user;
  isOpen: Boolean = false;
  contracts = [];
  contractsToshow = [];
  showMoreButton: Boolean = false;
  tabIndex: number = 0;
  additionalTabIndex: number = 2;
  foundDup: Boolean = false;
  activeContract;
  constructor() {}
  activateContract(contract){
    this.activeContract = contract;
    this.setActiveContractInDropdown(contract);
    
    this.isOpen = false;
    for ( var i = 0; i < this.contractsToshow.length; i++ ){
      if ( contract.id === this.contractsToshow[i].id ) {
        
        this.tabIndex = ( i>2 ) ? 0 : i;
        this.foundDup = true;
        break;
      }
    }
    if ( !this.foundDup ) {
      console.log(this.tabIndex)
      this.contractsToshow[this.tabIndex] = contract;
    }
    this.foundDup = false;
  }
  setActiveContractInDropdown(contract){
    for (let contract of this.contracts) {
      contract.active = false;
    }
    contract.active = true;
  }
  onTabChange(event){
    this.tabIndex = event.index;
    let id = this.contractsToshow[event.index].id;
    for (let contract of this.contracts) {
      if ( contract.id === id ) {
        this.activeContract = contract;
        this.setActiveContractInDropdown(contract);
        break;
      }
    }
  }
  onAdditionalTabChange(event){
    this.additionalTabIndex = event.index;
  }
  ngOnInit() {
    this.user = {
      name: 'Konstantin Valeshkovsky',
      image: 'http://placehold.it/100x100',
      phone: '+380631234567',
      email: 'longusername@longusernameservice.com',
      type: 'Payer',
      country: 'Ukraine',
      street: '10 Armeyskaya st.'
    }
    
    this.contracts = [{
      id: 129012987,
      msdism: '2332231564',
        status: 'Complete',
        type: 'Production',
        offer: 'CI Soft Launch',
        seniority: '190 (days)',
        city: 'Abidjan',
        street: '10 Armeyskaya st.',
        role: 'Main Payer'
      },
      {
        id: 129114983,
        msdism: '8345123453',
        status: 'Pending',
        type: 'Sandbox',
        offer: 'CI Soft Launch',
        seniority: '150 (days)',
        city: 'Odessa',
        street: '10 Armeyskaya st.',
        role: 'Secondary Payer'
      },
      {
        id: 194141775,
        msdism: '8345123453',
        status: 'Cancelled',
        type: 'Development',
        offer: 'CI Soft Launch',
        seniority: '2 (days)',
        city: 'Amsterdam',
        street: '10 Armeyskaya st.',
        role: 'None'
      },
      {
        id: 329497483,
        msdism: '8345123453',
        status: 'Pending',
        type: 'Sandbox',
        offer: 'CI Soft Launch',
        seniority: '290 (days)',
        city: 'Liverpool',
        street: '10 Armeyskaya st.',
        role: 'Shifter'
      }
    ]
    this.activeContract = this.contracts[0];
    if (this.contracts.length > 3) {
      this.menuItems = this.contracts;
      this.showMoreButton = true;
      this.contracts[0].active = true;
      this.contractsToshow = this.contracts.slice(0,3);
    }
  }
  
}