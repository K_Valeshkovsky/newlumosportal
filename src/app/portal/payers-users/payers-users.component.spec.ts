import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayersUsersComponent } from './payers-users.component';

describe('PayersUsersComponent', () => {
  let component: PayersUsersComponent;
  let fixture: ComponentFixture<PayersUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayersUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayersUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
