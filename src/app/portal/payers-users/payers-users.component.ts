import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-payers-users',
  templateUrl: './payers-users.component.html',
  styleUrls: ['./payers-users.component.scss']
})
export class PayersUsersComponent implements OnInit {
  @Input()
  contract;
  historyShown: Boolean = false;
  isOpen: Boolean;
  buttonText: string = "Show";
  debts;

  constructor() { }
  toggleDebtHistoryVisibility(){
    this.historyShown = !this.historyShown;
    this.buttonText = this.historyShown ? 'Hide' : 'Show';
  }
  refreshDebtHistory(){
    console.log('refreshed');
  }
  ngOnInit() {
    this.debts = [
      {date: new Date(2011,8,5), number:'1235427', status: 'New', type: 'Manage Debt', subtype: 'Change Debt'},
      {date: new Date(2011,3,12), number:'6234363', status: 'New', type: 'Manage Debt', subtype: 'Change Debt'},
      {date: new Date(2011,1,31), number:'9876543', status: 'Cancelled', type: 'Manage Debt', subtype: 'Change Debt'},
      {date: new Date(2011,11,16), number:'8865434', status: 'Pending', type: 'Manage Debt', subtype: 'Change Debt'},
      {date: new Date(2011,6,25), number:'6723457', status: 'Closed', type: 'Manage Debt', subtype: 'Change Debt'}
    ]
  }

}
