import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Sort } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

export interface Customer {
  name: string;
  img: string;
  createdOn: Date;
  phone: string;
  id: string;
  SPS: string;
  active: boolean;
}

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {
  filterOptions: {value:string, label:string}[];
  customers: Customer[] = [
    { name: 'Konstantin Valeshkovsky', img: 'http://placehold.it/100x100', createdOn: new Date(2018,12,1), phone: '+380123456789', id:'1243564752', SPS: '123532', active: false },
    { name: 'Dimitriy Kovalchuk', img: 'http://placehold.it/100x100', createdOn: new Date(2016,1,21), phone: '+380123001789', id:'6723543232', SPS: '321419', active: true },
    { name: 'Lida Choroshaya', img: 'http://placehold.it/100x100', createdOn: new Date(1995,5,12), phone: '+780133221781', id:'2345685643', SPS: '945356', active: true },
    { name: 'Albert Einstein', img: 'http://placehold.it/100x100', createdOn: new Date(1895,3,7), phone: '+380700331008', id:'0000123124', SPS: '999999', active: false },
    { name: 'Grek Zorba', img: 'http://placehold.it/100x100', createdOn: new Date(2000,6,6), phone: '+380126712700', id:'5566234927', SPS: '787325', active: true },
    { name: 'Koschey Bessmertniy', img: 'http://placehold.it/100x100', createdOn: new Date(1666,9,11), phone: '+999999999999', id:'6666666666', SPS: '312312', active: true },
    { name: 'Konstantin Valeshkovsky', img: 'http://placehold.it/100x100', createdOn: new Date(2018,12,1), phone: '+380123456789', id:'1243564752', SPS: '123532', active: false },
    { name: 'Dimitriy Kovalchuk', img: 'http://placehold.it/100x100', createdOn: new Date(2016,1,21), phone: '+380123001789', id:'6723543232', SPS: '321419', active: true },
    { name: 'Lida Choroshaya', img: 'http://placehold.it/100x100', createdOn: new Date(1995,5,12), phone: '+780133221781', id:'2345685643', SPS: '945356', active: true },
    { name: 'Albert Einstein', img: 'http://placehold.it/100x100', createdOn: new Date(1895,3,7), phone: '+380700331008', id:'0000123124', SPS: '999999', active: false },
    { name: 'Grek Zorba', img: 'http://placehold.it/100x100', createdOn: new Date(2000,6,6), phone: '+380126712700', id:'5566234927', SPS: '787325', active: true },
    { name: 'Koschey Bessmertniy', img: 'http://placehold.it/100x100', createdOn: new Date(1666,9,11), phone: '+999999999999', id:'6666666666', SPS: '312312', active: true }
  ];
  sortedData: Customer[];

  public searchForm: FormGroup;
  blockView: boolean = false;
  selected: string;

  constructor(private fb: FormBuilder, private route: ActivatedRoute,private router: Router) {
    this.sortedData = this.customers.slice();
   }
  sortData(sort: Sort) {
    const data = this.customers.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'created': return compare(a.createdOn, b.createdOn, isAsc);
        case 'phone': return compare(a.phone, b.phone, isAsc);
        case 'id': return compare(a.id, b.id, isAsc);
        case 'SPS': return compare(a.SPS, b.SPS, isAsc);
        default: return 0;
      }
    });
  }
  ngOnInit() {
    this.filterOptions = [
      {value:'Sales',label:'Sales'},
      {value:'Executive',label:'Executive'},
      {value:'Procurement',label:'Procurement'},
      {value:'Finance',label:'Finance'},
      {value:'Logistics',label:'Logistics'},
      {value:'Customer Service',label:'Customer Service'},
      {value:'Manufacturing',label:'Manufacturing'}
    ]
    this.searchForm = this.fb.group({
      blockView: this.fb.control(false),
      selected: this.fb.control('')
    })
    this.onChanges();
  }
  toggleSort() {
    this.blockView = !this.blockView;
  }
  onChanges(): void {
    this.searchForm.valueChanges.subscribe(formValue => {
      console.log(formValue);
    });
  }
  onFormSubmit(formValue){
    console.log(formValue)
  }
  showCustomer(customer){
    this.router.navigate(['../dashboard'], {relativeTo: this.route});
  }
}
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}