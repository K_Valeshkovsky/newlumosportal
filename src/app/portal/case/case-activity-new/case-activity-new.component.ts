import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { SelectItem } from 'primeng/api';
@Component({
  selector: 'app-case-activity-new',
  templateUrl: './case-activity-new.component.html',
  styleUrls: ['./case-activity-new.component.scss']
})
export class CaseActivityNewComponent implements OnInit {
  newActivityForm;
  types;
  requests;
  departments;
  statuses;
  explanation: string = '';
  explanationMaxLength: number = 120;

  constructor(private fb: FormBuilder) { }
  cancelAddingActivity() {
    console.log('cancelled');
  }
  onSubmit(){
    console.log(this.newActivityForm);
  }
  ngOnInit() {
    this.types= [
      {label:'Type 1',value:'Type 1'},
      {label:'Type 2',value:'Type 2'},
      {label:'Type 3',value:'Type 3'}
    ]
    this.requests= [
      {label:'Request 1',value:'Request 1'},
      {label:'Request 2',value:'Request 2'},
      {label:'Request 3',value:'Request 3'}
    ]
    this.departments= [
      {label:'Department 1',value:'Department 1'},
      {label:'Department 2',value:'Department 2'},
      {label:'Department 3',value:'Department 3'}
    ]
    this.statuses= [
      {label:'Status 1',value:'Status 1'},
      {label:'Status 2',value:'Status 2'},
      {label:'Status 3',value:'Status 3'}
    ]
    this.newActivityForm = new FormGroup({
      'type': new FormControl(''),
      'request': new FormControl(''),
      'department': new FormControl(''),
      'status': new FormControl(''),
      'explanation': new FormControl('')
    });
  }

}
