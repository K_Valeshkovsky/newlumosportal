import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseActivityNewComponent } from './case-activity-new.component';

describe('CaseActivityNewComponent', () => {
  let component: CaseActivityNewComponent;
  let fixture: ComponentFixture<CaseActivityNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseActivityNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseActivityNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
