import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-case-activities-list',
  templateUrl: './case-activities-list.component.html',
  styleUrls: ['./case-activities-list.component.scss']
})
export class CaseActivitiesListComponent implements OnInit {
  activities;
  constructor() { }

  ngOnInit() {
    this.activities=[
      {
        date: new Date(2018,6,21),
        author: 'Konstantin Valeshkovsky',
        position: 'System Admin',
        type: 'Retrieval Visit',
        status: 'Pending',
        result: 'Cancelled Membership'
      },
      {
        date: new Date(2018,3,13),
        author: 'Bruce Waine',
        position: 'Super User',
        type: 'Retrieval Visit',
        status: 'Done',
        result: 'Escalated'
      },
      {
        date: new Date(2017,4,5),
        author: 'Deadpool',
        position: 'Just passed by',
        type: 'Unrecognized',
        status: 'Done',
        result: 'Escalated'
      },
      {
        date: new Date(2018,1,16),
        author: 'Konstantin Valeshkovsky',
        position: 'Nobody',
        type: 'Visit',
        status: 'Cancelled',
        result: 'None'
      },
      {
        date: new Date(2018,1,15),
        author: 'Konstantin Valeshkovsky',
        position: 'System Admin',
        type: 'Retrieval Visit',
        status: 'Pending',
        result: 'Cancelled Membership'
      },
      {
        date: new Date(2018,1,15),
        author: 'Konstantin Valeshkovsky',
        position: 'System Admin',
        type: 'Retrieval Visit',
        status: 'Pending',
        result: 'Cancelled Membership'
      }
    ]
  }

}
