import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseActivitiesListComponent } from './case-activities-list.component';

describe('CaseActivitiesListComponent', () => {
  let component: CaseActivitiesListComponent;
  let fixture: ComponentFixture<CaseActivitiesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseActivitiesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseActivitiesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
