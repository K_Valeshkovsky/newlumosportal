import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-case-additional',
  templateUrl: './case-additional.component.html',
  styleUrls: ['./case-additional.component.scss']
})
export class CaseAdditionalComponent implements OnInit {
  tabIndex: number = 1;

  constructor() { }

  ngOnInit() {
  }
  onTabChange(event){
    this.tabIndex = event.index;
  }
}
