import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gages',
  templateUrl: './gages.component.html',
  styleUrls: ['./gages.component.scss']
})
export class GagesComponent implements OnInit {
  tabIndex: number;
  days;
  currentChargeState;
  currentLoad: number;
  constructor() { }

  onTabChange(event){
    this.tabIndex = event.index;
  }
  calcHeight(num){
    return "linear-gradient(to top, #F8CF1C 0%,  #F8CF1C " + num/100*85 + "%" + ', #f3f3f3 ' + num/100*85 + "%, transparent 85%"
  }
  calcAngle(num){
    // 0deg = 0%
    // 180deg = 100%
    // 
    return {'transform': 'rotate('+1.8*num+"deg)"}
  }
  loadColor(){
    return  this.currentLoad > 65 ? '#DE4846' : this.currentLoad > 33 ? "#F8CF1C" : "#58b93f"
  }
  ngOnInit() {
    this.currentLoad=30;
    this.tabIndex = 3;
    this.currentChargeState = 25;
    this.days = [
      new Date(2018, 6, 12), 
      new Date(2018, 6, 13),
      new Date(2018, 6, 14),
      new Date(2018, 6, 15),
      new Date(2018, 6, 16),
      new Date(2018, 6, 17),
      new Date(2018, 6, 18),
      new Date(2018, 6, 19)
    ]
    console.log(this.days);
    this.days = this.days.slice(-5);

  }

}
