import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-case-list',
  templateUrl: './case-list.component.html',
  styleUrls: ['./case-list.component.scss']
})
export class CaseListComponent implements OnInit {
  newActivity: Boolean = false;
  menuItems: MenuItem[];
  isOpen: Boolean = false;
  cases = [];
  casesToshow = [];
  showMoreButton: Boolean = false;
  tabIndex: number = 0;
  foundDup: Boolean = false;
  activeCase;
  constructor() { }
  activateCase(customerCase){
    this.activeCase = customerCase;
    this.setActiveCaseInDropdown(customerCase);
    
    this.isOpen = false;
    for ( var i = 0; i < this.casesToshow.length; i++ ){
      if ( customerCase.id === this.casesToshow[i].id ) {
        
        this.tabIndex = ( i>2 ) ? 0 : i;
        this.foundDup = true;
        break;
      }
    }
    if ( !this.foundDup ) {
      console.log(this.tabIndex)
      this.casesToshow[this.tabIndex] = customerCase;
    }
    this.foundDup = false;
  }
  onTabChange(event){
    this.tabIndex = event.index;
    let id = this.casesToshow[event.index].id;
    for (let customerCase of this.cases) {
      if ( customerCase.id === id ) {
        this.activeCase = customerCase;
        this.setActiveCaseInDropdown(customerCase);
        break;
      }
    }
  }
  setActiveCaseInDropdown(customerCase){
    for (let customerCase of this.cases) {
      customerCase.active = false;
    }
    customerCase.active = true;
  }
  showArchive() {
    console.log('Archive is shown');
  }
  addActivity(){
    this.newActivity = true;
    // console.log('Activity added');
  }
  ngOnInit() {
    this.cases = [
      {
        id: '00660200',
        name: 'Konstantin Valeshkovsky',
        status: 'Production',
        type: 'Paid',
        subtype: 'Uncommon',
        complaints: [
          'Change Customer Name', 'Change Customer Name'
        ]
      },
      {
        id: '00060273',
        name: 'Konstantin Valeshkovsky',
        status: 'Production',
        type: 'Paid',
        subtype: 'Uncommon',
        complaints: [
          
        ]
      },
      {
        id: '02090030',
        name: 'Konstantin Valeshkovsky',
        status: 'Production',
        type: 'Paid',
        subtype: 'Uncommon',
        complaints: [
          'I dont like it', 'Strange Text'
        ]
      },
      {
        id: '30256013',
        name: 'Konstantin Valeshkovsky',
        status: 'Production',
        type: 'Paid',
        subtype: 'Uncommon',
        complaints: [
          'Change Paid Status', 'Change Anything'
        ]
      }
    ]
    this.activeCase = this.cases[0];
    if (this.cases.length > 3) {
      this.menuItems = this.cases;
      this.showMoreButton = true;
      this.cases[0].active = true;
      this.casesToshow = this.cases.slice(0,3);
    }
  }
  closeCase(){
    console.log("Case closed");
  }
}
