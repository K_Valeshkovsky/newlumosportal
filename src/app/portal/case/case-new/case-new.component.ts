import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'node_modules/primeng/api';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-case-new',
  templateUrl: './case-new.component.html',
  styleUrls: ['./case-new.component.scss']
})
export class CaseNewComponent implements OnInit {
  newCaseForm: FormGroup;
  cases;
  activeCase;
  showMoreButton: Boolean = false;
  menuItems: MenuItem[];
  isOpen: Boolean = false;
  casesToshow;
  constructor(private fb: FormBuilder) { }
  onSubmit(){
    console.log(this.newCaseForm);
  }
  cancel(){
    console.log('Not saved Form');
    this.newCaseForm.reset();
  }
  ngOnInit() {
    this.newCaseForm = new FormGroup({
      'username': new FormControl('', [Validators.required]),
      'type': new FormControl('', [Validators.required]),
      'subtype': new FormControl('', [Validators.required]),
      'status': new FormControl('', [Validators.required]),
      'complaint1': new FormControl('', [Validators.required]),
      'complaint2': new FormControl('', [Validators.required]),
    });
    this.newCaseForm.statusChanges.subscribe(
      (value) => {
        console.log(value);
      }
    )
    this.cases = [
      {
        id: '00660200',
        name: 'Konstantin Valeshkovsky',
        status: 'Production',
        type: 'Paid',
        subtype: 'Uncommon',
        complaints: [
          'Change Customer Name', 'Change Customer Name'
        ]
      },
      {
        id: '00060273',
        name: 'Konstantin Valeshkovsky',
        status: 'Production',
        type: 'Paid',
        subtype: 'Uncommon',
        complaints: [
          
        ]
      },
      {
        id: '02090030',
        name: 'Konstantin Valeshkovsky',
        status: 'Production',
        type: 'Paid',
        subtype: 'Uncommon',
        complaints: [
          'I dont like it', 'Strange Text'
        ]
      },
      {
        id: '30256013',
        name: 'Konstantin Valeshkovsky',
        status: 'Production',
        type: 'Paid',
        subtype: 'Uncommon',
        complaints: [
          'Change Paid Status', 'Change Anything'
        ]
      }
    ]
    if (this.cases.length > 3) {
      this.menuItems = this.cases;
      this.showMoreButton = true;
      this.casesToshow = this.cases.slice(0,2);
    }
  }

}
