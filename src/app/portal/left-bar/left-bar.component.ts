import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';




@Component({
  selector: 'app-left-bar',
  templateUrl: './left-bar.component.html',
  styleUrls: ['./left-bar.component.scss']
})

export class LeftBarComponent implements OnInit {
  @ViewChild('sidenav') sidenav: MatSidenav;
  
  showSubmenu: boolean = false;
  
  navLinks = [
    {
      name : 'Sales',
      img: 'home',
      isExpanded : true,
      isShowing : false,
      showSubmenu: false,
      childs : [
        { name: 'Pre-Registration', link: 'pre-registration' },
        { name: 'Screening Status', link: 'screening-status' },
        { name: 'Registration', link: 'registration' },
        { name: 'Store Leads', link: 'store-leads' }
      ]
    },
    {
      name : 'Service',
      img: 'home',
      isExpanded : true,
      isShowing : false,
      showSubmenu: false,
      childs : [
        { name: 'Pending Approvals', link: 'pending-approvals' }
      ]
    },
    {
      name : 'Operational',
      img: 'home',
      isExpanded : true,
      isShowing : false,
      showSubmenu: false,
      childs : [
        { name: 'Reports Stock In', link: 'reports-stock-in' },
        { name: 'Report DOA In store', link: 'report-doa-in-store' },
        { name: 'Inventory Counting', link: 'inventory-counting' },
        { name: 'Fault After Lab Repair', link: 'fault-after-lab-repair' },
        { name: 'Demo Units Activation', link: 'demo-units-activation' },
  
      ]
    }
  ]

  constructor() {
  }

  ngOnInit() {

  }

}
