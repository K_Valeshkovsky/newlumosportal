import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-assets',
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.scss']
})
export class AssetsComponent implements OnInit {
  @Input()
  contract;

  assets;
  constructor(
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.assets = [
      {name: 'B', group: 'SPS', description: ''},
      {name: '1', group: 'Rate Plan', description: ''},
      {name: '75W Car Power Inverter', group: 'Appliances', description: '75W DC2AC Converter, with Two USB Ports 983020939039'},
      {name: '30K Setup Fee', group: 'Setup Fee', description: ''}
    ]
  }
  openNewCase(){
    console.log('New case Opened');
    this.router.navigate(['/case/new/']);
  }
}
