import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentsActivationsComponent } from './payments-activations.component';

describe('PaymentsActivationsComponent', () => {
  let component: PaymentsActivationsComponent;
  let fixture: ComponentFixture<PaymentsActivationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentsActivationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentsActivationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
