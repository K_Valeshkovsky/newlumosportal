import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-payments-activations',
  templateUrl: './payments-activations.component.html',
  styleUrls: ['./payments-activations.component.scss']
})
export class PaymentsActivationsComponent implements OnInit {
  @Input()
  contract;
  
  stepperItems: {label:String, status: String}[]
  operations;
  constructor() { }

  ngOnInit() {
    this.operations = [1,2]
    this.stepperItems = [
        {label: 'Step 1', status:'Done'},
        {label: 'Step 2', status:'Error'},
        {label: 'Step 3', status:'Active'},
        {label: 'Step 4', status:''},
        {label: 'Step 5', status:'Done'}
    ];
  }
  addAppliance(){
    console.log('here');
  }
}
