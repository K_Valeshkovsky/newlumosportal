import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cases',
  templateUrl: './cases.component.html',
  styleUrls: ['./cases.component.scss']
})
export class CasesComponent implements OnInit {
  @Input()
  contract;
  cases;
  constructor() { }

  ngOnInit() {
    this.cases = [
      {date: new Date(2011,8,5), number:'1235427', status: 'New', type: 'Manage Debt', subtype: 'Change Debt'},
      {date: new Date(2011,3,12), number:'6234363', status: 'New', type: 'Manage Debt', subtype: 'Change Debt'},
      {date: new Date(2011,1,31), number:'9876543', status: 'Cancelled', type: 'Manage Debt', subtype: 'Change Debt'},
      {date: new Date(2011,11,16), number:'8865434', status: 'Pending', type: 'Manage Debt', subtype: 'Change Debt'},
      {date: new Date(2011,6,25), number:'6723457', status: 'Closed', type: 'Manage Debt', subtype: 'Change Debt'}
    ]
  }
  openNewCase(){
    console.log('here');
  }
}
