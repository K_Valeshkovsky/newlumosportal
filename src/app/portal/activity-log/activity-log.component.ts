import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-activity-log',
  templateUrl: './activity-log.component.html',
  styleUrls: ['./activity-log.component.scss']
})
export class ActivityLogComponent implements OnInit {
  @Input()
  contract;
  logs: {type: string, text: string}[];
  constructor() { }

  ngOnInit() {
    this.logs = [
      {type: 'SMS', text: 'Line Type: SMS'},
      {type: 'Activation', text: 'Activated Set: IDU: 124902; Panel: 000000000000000080045b2 Deactivated Set'},
      {type: 'SMS', text: 'Line Type: SMS'},
      {type: 'Activation', text: 'Activated Set: IDU: 124902; Panel: 000000000000000080045b2 Deactivated Set'},
      {type: 'SMS', text: 'Line Type: SMS'},
      {type: 'Activation', text: 'Activated Set: IDU: 124902; Panel: 000000000000000080045b2 Deactivated Set'},
      {type: 'SMS', text: 'Line Type: SMS'},
      {type: 'Activation', text: 'Activated Set: IDU: 124902; Panel: 000000000000000080045b2 Deactivated Set'},
      {type: 'SMS', text: 'Line Type: SMS'},
      {type: 'Activation', text: 'Activated Set: IDU: 124902; Panel: 000000000000000080045b2 Deactivated Set'},
      {type: 'SMS', text: 'Line Type: SMS'},
      {type: 'Activation', text: 'Activated Set: IDU: 124902; Panel: 000000000000000080045b2 Deactivated Set'},
      {type: 'SMS', text: 'Line Type: SMS'},
      {type: 'Activation', text: 'Activated Set: IDU: 124902; Panel: 000000000000000080045b2 Deactivated Set'},
      {type: 'SMS', text: 'Line Type: SMS'},
      {type: 'Activation', text: 'Activated Set: IDU: 124902; Panel: 000000000000000080045b2 Deactivated Set'},
      {type: 'SMS', text: 'Line Type: SMS'},
      {type: 'Activation', text: 'Activated Set: IDU: 124902; Panel: 000000000000000080045b2 Deactivated Set'},
      {type: 'SMS', text: 'Line Type: SMS'},
      {type: 'Activation', text: 'Activated Set: IDU: 124902; Panel: 000000000000000080045b2 Deactivated Set'},
    ]
  }

}
