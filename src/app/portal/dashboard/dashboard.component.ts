import { Component, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr'; // 1

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private toastr: ToastrService) { } // 2

  ngOnInit() {
    // this.toastSuccess('Hi, username. This is a very long message for test purposes. You can also close this message by clicking button to the right.');
    // this.toastError('Hi');
    // this.toastInfo('Hi');
    // this.toastWarning('Hi');
  }

  toastSuccess(message = '',header?) { // 3
    this.toastr.success(message, header); 
  }

  toastError(message = '',header?) { // 3
    this.toastr.error(message, header);
  }

  toastInfo(message = '',header?) { // 3
    this.toastr.info(message, header); 
  }

  toastWarning(message = '',header?) { // 3
    this.toastr.warning(message, header); 
  }
}
