import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sps-panel',
  templateUrl: './sps-panel.component.html',
  styleUrls: ['./sps-panel.component.scss']
})
export class SpsPanelComponent implements OnInit {
  @Input()
  inCaseView:Boolean;

  current: number;
  min: number;
  max: number;
  circleNumbers = [];
  constructor() { }

  ngOnInit() {
    this.current = 467;
    this.max = 1800;
    this.min = this.max / 8;
    for (var i = 1; i <= 8; i++) { // Creating number labels
      this.circleNumbers.push(this.min*i);
    }
    console.log(this.circleNumbers);
  }

}
