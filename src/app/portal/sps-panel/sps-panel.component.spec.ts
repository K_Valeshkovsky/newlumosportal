import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpsPanelComponent } from './sps-panel.component';

describe('SpsPanelComponent', () => {
  let component: SpsPanelComponent;
  let fixture: ComponentFixture<SpsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
