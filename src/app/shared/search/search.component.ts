import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, FormBuilder} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

/**
 * @title Filter autocomplete
 */
@Component({
  selector: 'app-search',
  templateUrl: 'search.component.html',
  styleUrls: ['search.component.scss'],
})
export class SearchComponent implements OnInit {
  searchValues: string[];
  maxValues: number = 3;
  public searchForm: FormGroup;

  constructor( 
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router ){}
  onSubmit(form){
    console.log(form.value);
    this.router.navigate(['/search-result']);
  }
  onKeypress(e){
    if ( e.keyCode == 13 || this.searchForm.value.searchValues.length === this.maxValues) { //enter
      e.preventDefault();
      this.onSubmit(this.searchForm);
    }
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      searchValues: this.fb.control([])
    })
  }

}
