import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClickOutsideDirective } from "./shared/click-outside.directive";

import { DropdownModule } from 'primeng/dropdown';
import { ChipsModule } from 'primeng/chips';
import { TabViewModule } from 'primeng/tabview';
import { MenuModule } from 'primeng/menu';
import { StepsModule } from 'primeng/steps';
import { MenuItem } from 'primeng/api';
import { AccordionModule } from 'primeng/accordion';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { GrowlModule } from 'primeng/growl';
import { ChartModule } from 'primeng/chart';

import { StaticPathPipe } from './static-path.pipe';

import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { 
  MatButtonModule,
  MatMenuModule,
  MatAutocompleteModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatOptionModule,
  MatSlideToggleModule,
  ErrorStateMatcher,
  ShowOnDirtyErrorStateMatcher, 
  MatSidenavModule,
  MatListModule,
  MatTreeModule,
  MatTableModule,
  MatSortModule
} from '@angular/material';

import { MatCheckboxModule } from '@angular/material/checkbox';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgModule } from '@angular/core';

import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './auth/login/login.component';
import { AuthComponent } from './auth/auth.component';
import { AppComponent } from './app.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { PortalComponent } from './portal/portal.component';
import { LeftBarComponent } from './portal/left-bar/left-bar.component';
import { TopBarComponent } from './portal/top-bar/top-bar.component';
import { DashboardComponent } from './portal/dashboard/dashboard.component';
import { SearchComponent } from './shared/search/search.component';
import { UserProfileComponent } from './portal/user-profile/user-profile.component';
import { IconsComponent } from './shared/icons/icons.component';
import { UserProfileDropdownComponent } from './shared/user-profile-dropdown/user-profile-dropdown.component';
import { SearchResultComponent } from './portal/search-result/search-result.component';
import { ActivityLogComponent } from './portal/activity-log/activity-log.component';
import { PayersUsersComponent } from './portal/payers-users/payers-users.component';
import { PaymentsActivationsComponent } from './portal/payments-activations/payments-activations.component';
import { CasesComponent } from './portal/cases/cases.component';
import { AssetsComponent } from './portal/assets/assets.component';
import { SpsPanelComponent } from './portal/sps-panel/sps-panel.component';
import { CaseComponent } from './portal/case/case.component';
import { CaseAdditionalComponent } from './portal/case/case-additional/case-additional.component';
import { GraphsComponent } from './portal/case/case-additional/graphs/graphs.component';
import { BitComponent } from './portal/case/case-additional/bit/bit.component';
import { GagesComponent } from './portal/case/case-additional/gages/gages.component';
import { CaseListComponent } from './portal/case/case-list/case-list.component';
import { CaseNewComponent } from './portal/case/case-new/case-new.component';
import { CaseActivitiesListComponent } from './portal/case/case-activities-list/case-activities-list.component';
import { CaseActivityNewComponent } from './portal/case/case-activity-new/case-activity-new.component';
import { ModalComponent } from './shared/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    LoginComponent,
    ClickOutsideDirective,
    ForgotPasswordComponent,
    PortalComponent,
    LeftBarComponent,
    TopBarComponent,
    DashboardComponent,
    SearchComponent,
    UserProfileComponent,
    IconsComponent,
    UserProfileDropdownComponent,
    SearchResultComponent,
    ActivityLogComponent,
    PayersUsersComponent,
    PaymentsActivationsComponent,
    CasesComponent,
    AssetsComponent,
    SpsPanelComponent,
    CaseComponent,
    CaseAdditionalComponent,
    GraphsComponent,
    BitComponent,
    GagesComponent,
    CaseListComponent,
    CaseNewComponent,
    CaseActivitiesListComponent,
    CaseActivityNewComponent,
    ModalComponent,
    StaticPathPipe
  ],
  imports: [
    AppRoutingModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      maxOpened: 5,
      closeButton: true,
      disableTimeOut: true,
      tapToDismiss: false,
      timeOut: 0,
      positionClass: 'toast-top-left',
    }),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatMenuModule,
    MatAutocompleteModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatIconModule,
    MatListModule,
    MatTreeModule,
    MatCardModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatOptionModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    
    DropdownModule,
    ChipsModule,
    TabViewModule,
    MenuModule,
    StepsModule,
    AccordionModule,
    RoundProgressModule,
    InputTextModule,
    InputTextareaModule,
    GrowlModule,
    ChartModule

  ],
  exports: [
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatOptionModule,
    MatSlideToggleModule     
  ],
  providers: [
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
